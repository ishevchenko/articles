<?php

require_once 'Article.php';

class ArticleSocialTest extends PHPUnit_Framework_TestCase
{

    protected $article;

    protected function setUp()
    {
        $this->article = Article::fromUrl('http://siliconrus.com/2014/04/cmtt-coder/');
    }

    public function testLoadTwitterMentions()
    {
        $this->article->loadTwitterMentions();
        $this->assertTrue($this->article->twitterMentions > 0);
    }

    public function testLoadFacebookMentions()
    {
        $this->article->loadFacebookMentions();
        $this->assertTrue($this->article->facebookMentions > 0);
    }

    public function testLoadVkMentions()
    {
        $this->article->loadVkMentions();
        $this->assertTrue($this->article->vkMentions > 0);
    }

    public function testGetPublisher()
    {
        $this->assertEquals('siliconrus.com', $this->article->getPublisher());
    }
}