<?php

require_once 'Article.php';

class ArticleContentTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider providerLoadContent
     */
    public function testLoadContent($url)
    {
        print $url."\n";
        $article = Article::fromUrl($url);
        $article->loadContent();
        $this->assertNotNull($article->title);
        $this->assertNotNull($article->description);
    }

    public function providerLoadContent()
    {
        // По одной статье из СМИ, получаемых через API. Убедимся, что на всех сайтах есть нужные метатеги.
        return array(
            array('http://lifenews.ru/news/132506'),
            array('http://ria.ru/society/20140428/1005788370.html'),
            array('http://www.interfax.ru/world/372950'),
            array('http://www.gazeta.ru/politics/news/2014/05/02/n_6124749.shtml'),
            array('http://russian.rt.com/article/30055'),
            array('http://www.svoboda.org/content/article/25370399.html'),
            array('http://top.rbc.ru/society/22/04/2014/919580.shtml'),
            array('http://lenta.ru/news/2014/04/29/vk/'),
            array('http://www.novayagazeta.ru/politics/63402.html'),
            array('http://izvestia.ru/news/570284'),
            array('http://www.kp.ru/quiz/567/'),
            array('http://www.ntv.ru/novosti/944876/'),
            array('http://slon.ru/russia/gkchp_2_ili_russkoe_vesennee_obostrenie-1087849.xhtml'),
            array('http://tvrain.ru/articles/pavel_durov_uvolen_s_posta_gendirektora_vkontakte-367277/'),
            array('http://www.kommersant.ru/doc/2464879'),
            array('http://www.forbes.ru/forbeslife-photogallery/dosug/256037-101-i-kilometr-desyat-neobychnykh-marshrutov-v-podmoskove/photo/1'),
            array('http://www.dw.de/the-guardian-%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%BE%D0%BA%D0%BE%D0%B5%D0%BD%D0%B0-%D0%BD%D0%B0%D0%BF%D0%BB%D1%8B%D0%B2%D0%BE%D0%BC-%D0%BA%D0%BE%D0%BC%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%80%D0%B8%D0%B5%D0%B2-%D0%BF%D1%80%D0%BE%D1%80%D0%BE%D1%81%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%BE%D0%B3%D0%BE-%D1%85%D0%B0%D1%80%D0%B0%D0%BA%D1%82%D0%B5%D1%80%D0%B0/a-17612458'),
            array('http://inosmi.ru/world/20140505/220032210.html'),
            array('http://www.vedomosti.ru/library/news/26101621/imidzh-gosudarstvennogo-znacheniya'),
            array('http://www.znak.com/svrdl/news/2014-05-05/1022099.html'),
            array('http://www.rosbalt.ru/ukraina/2014/04/29/1262988.html'),
            array('http://www.rg.ru/2014/05/05/slavyansk-site-anons.html'),
            array('http://www.newsru.com/finance/05may2014/cb.html'),
            array('http://www.bbc.co.uk/russian/russia/2014/04/140423_russia_internet_law.shtml')
        );
    }
}