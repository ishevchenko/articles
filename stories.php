<?php

require_once 'vendor/autoload.php';
require_once 'vendor/nxp/russian-porter-stemmer/src/NXP/Stemmer.php';

use \NlpTools\Similarity\CosineSimilarity;
use \NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;

/*
 * Преобразование заголовка и описания статьи в список токенов.
 * Токены обрабатываются стеммером (то есть отбрасываются окончания слов).
 */
function getTokens($article)
{
    $tokenizer = new WhitespaceAndPunctuationTokenizer();
    $tokens = $tokenizer->tokenize($article->title . ' ' . $article->description);

    $stemmer = new \Nxp\Stemmer();

    $stemmed = [];
    foreach($tokens as $token) {
        $stem = $stemmer->getWordBase($token);
        if (strlen($stem) > 2){
            $stemmed[] = $stem;
        }
    }
    return $stemmed;
}

/*
 * Поиск самой похожей статьи среди существующих статей.
 * Если похожих статей нет, возвращается null.
 */
function getSimilarArticle($article, $oldArticles)
{
    $mostSimilarArticle = null;
    $bestSimilarity = 0;

    // В качестве меры похожести используется косинусное расстояние.
    // Было бы хорошо еще взвешивать токены, используя tf-idf, чтобы более редкие термины имели больший вес.
    $cos = new CosineSimilarity();

    $articleTokens = getTokens($article);
    foreach($oldArticles as $oldArticle) {
        $oldArticleTokens = getTokens($oldArticle);
        if (!($articleTokens && $oldArticleTokens)) continue;
        $similarity = $cos->similarity($articleTokens, $oldArticleTokens);
        if ($similarity > $bestSimilarity) {
            $mostSimilarArticle = $oldArticle;
            $bestSimilarity = $similarity;
        }
    }

    // 0.25 - экспериментально подобранное значение, при похожести ниже которого статьи не объединяются в один сюжет.
    // В идеале нужно найти вручную некоторое количество сюжетов и обучить на них классификатор (например, SVM),
    // который уже будет принимать решение, объединять ли статьи в сюжет. Заодно там можно будет использовать и другие
    // фичи (например, временной промежуток между статьями).
    return ($bestSimilarity > 0.25)? $mostSimilarArticle : null;
}

/*
 * Поиск и присоединение новой статьи к сюжетам.
 */
function findStory($db, $oldArticles, $article)
{
    $similarArticle = getSimilarArticle($article, $oldArticles);
    if (!$similarArticle) return;

    if (!$similarArticle->belongsToStory()) {
        // Если старая статья не принадлежит никакому сюжету, нужно создать новый сюжет.
        $similarArticle->createStory($db);
        $similarArticle->save($db);
    }
    $article->addToStory($similarArticle->storyId);
}