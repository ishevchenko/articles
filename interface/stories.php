<?php
// Отображение списка сюжетов или, если есть параметр id, конкретного сюжета

require_once '../vendor/autoload.php';
require_once '../Database.php';
require_once '../config.php';

$db = new Database($dbConnectionString, $dbUsername, $dbPassword);

if (isset($_GET['id'])) {
    $articles = $db->getArticlesForStory($_GET['id']);

    $smarty = new Smarty;
    $smarty->assign('articles', $articles);
    $smarty->display('../templates/story.tpl');
} else {
    $articles = $db->getStoriesForDisplay();

    $smarty = new Smarty;
    $smarty->assign('articles', $articles);
    $smarty->display('../templates/story_list.tpl');
}