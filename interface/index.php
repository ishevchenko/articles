<?php
// Отображение списка статей.

require_once '../vendor/autoload.php';
require_once '../Database.php';
require_once '../config.php';

$db = new Database($dbConnectionString, $dbUsername, $dbPassword);

$articles = $db->getArticlesForDisplay();

$smarty = new Smarty;
$smarty->assign('articles', $articles);
$smarty->display('../templates/article_list.tpl');