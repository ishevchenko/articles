<?php

$API_URL = 'https://api.tjournal.ru/1/vacancy';

/*
 * Получение ссылки на статью через апи.
 */
function getArticleUrl()
{
    global $API_URL;

    $contents = file_get_contents($API_URL);
    $response = json_decode($contents);

    return $response->{'url'};
}