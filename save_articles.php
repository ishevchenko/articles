<?php
/*
 * Добавление в базу новых статей, получаемых через апи.
 */

require_once 'Article.php';
require_once 'Database.php';
require_once 'api.php';
require_once 'stories.php';
require_once 'config.php';

$db = new Database($dbConnectionString, $dbUsername, $dbPassword);

$oldArticles = $db->getArticlesForStoryDetection();

for ($i = 0; $i < $saveAtOnce; $i++) {
    $url = getArticleUrl();

    print $url;

    if ($db->urlIsKnown($url)) {
        print " already saved\n";
    } else {
        $article = Article::fromUrl($url);
        try {
            $article->loadAllInformation();         // Загрузка информации о статье.
            findStory($db, $oldArticles, $article); // Присоединение статьи к сюжету.
            $article->save($db);                    // Сохранение.
            print " saved\n";
            $oldArticles[] = $article;
        } catch (Exception $e) {
            // Что-то не получилось, попробуем в другой раз.
            print $e."\n";
        }
    }
}