<?php
/*
 * Обновление информации об упоминаниях в существующих статьях.
 */

require_once 'Database.php';
require_once 'config.php';

$db = new Database($dbConnectionString, $dbUsername, $dbPassword);

foreach($db->getArticlesForUpdate() as $article) {
    print $article->url;
    $article->loadSocialMentions();
    $article->save($db);
    print ' updated'.'\n';
}