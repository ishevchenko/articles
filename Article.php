<?php

require_once 'vendor/autoload.php';

// Притворяемся браузером, чтобы работать с некоторыми сайтами (например, kp.ru без этого отдает 404).
ini_set('user_agent', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0');

class Article
{

    public $id;
    public $url;
    public $twitterMentions;
    public $facebookMentions;
    public $vkMentions;
    public $title;
    public $description;
    public $imageUrl;
    public $createdDate;
    public $updatedDate;
    public $storyId;

    private function __construct($id, $url, $title, $description, $imageUrl, $twitterMentions,
                                 $facebookMentions, $vkMentions, $createdDate, $updatedDate, $storyId)
    {
        $this->id = $id;
        $this->url = $url;
        $this->title = $title;
        $this->description = $description;
        $this->imageUrl = $imageUrl;
        $this->twitterMentions = $twitterMentions;
        $this->facebookMentions = $facebookMentions;
        $this->vkMentions = $vkMentions;
        $this->createdDate = $createdDate;
        $this->updatedDate = $updatedDate;
        $this->storyId = $storyId;
    }

    public static function fromUrl($url)
    {
        return new Article(null, $url, null, null, null, 0, 0, 0, date('Y-m-d H:i:s'), null, null);
    }

    public static function fromDatabase($id, $url, $title, $description, $imageUrl, $twitterMentions,
                                        $facebookMentions, $vkMentions, $createdDate, $updatedDate, $storyId)
    {
        return new Article($id, $url, $title, $description, $imageUrl, $twitterMentions,
            $facebookMentions, $vkMentions, $createdDate, $updatedDate, $storyId);
    }

    /*
     * Сохранение статьи в БД: создание новой или обновление существующей.
     */
    public function save($db)
    {
        $this->updatedDate = date('Y-m-d H:i:s');
        if ($this->id) {
            $db->updateArticle($this);
        } else {
            $this->id = $db->createArticle($this);
        }
    }

    /*
     * Загрузка количества упоминаний в Твитере.
     */
    public function loadTwitterMentions()
    {
        // Этот способ получать количество упоминаний официально не поддерживается, но он самый удобный.
        $twitter_url = 'http://urls.api.twitter.com/1/urls/count.json?url='.$this->url;

        $contents = file_get_contents($twitter_url);
        $response = json_decode($contents);

        $this->twitterMentions = $response->{'count'};
    }

    /*
     * Загрузка количества упоминаний в Фейсбуке.
     */
    public function loadFacebookMentions()
    {
        $query = 'select share_count from link_stat where url="'.$this->url.'"';
        $facebook_url = 'http://api.facebook.com/method/fql.query?query='.urlencode($query);

        $xml = file_get_html($facebook_url);
        $share_count = $xml->find('share_count', 0)->plaintext;

        $this->facebookMentions = $share_count;
    }

    /*
     * Загрузка количества упоминаний во Вконтакте.
     */
    public function loadVkMentions()
    {
        $vk_url = 'http://vk.com/share.php?act=count&index=1&url='.$this->url;

        $contents = file_get_contents($vk_url);
        preg_match('/VK\.Share\.count\(1, (\d+)\)/', $contents, $result);

        $this->vkMentions = $result[1];
    }

    /*
     * Загрузка заголовка, описания и иллюстрации для статьи.
     * Для этого используется разметка Open Graph, если она есть.
     * Open Graph лучше других вариантов, потому что там обычно нет мусора вроде названия СМИ.
     */
    public function loadContent()
    {
        $dom = file_get_html($this->url);

        $og_title = $dom->find('meta[property="og:title"]', 0);
        if ($og_title) {
            $this->title = $og_title->content;
        } else {
            // Фолбэк для заголовка: тэг <title> страницы.
            $title_tag = $dom->find('title', 0);
            if ($title_tag) {
                $this->title = $title_tag->plaintext;
            } else {
                // Если нет даже тэга <title>, то со страницей что-то не так.
                // Попробуем сохранить ее в другой раз, а сейчас бросим исключение.
                throw new Exception('Bad page');
            }
        }

        $og_description = $dom->find('meta[property="og:description"]', 0);
        if ($og_description) {
            $this->description = $og_description->content;
        } else {
            // Фолбэк для описания: метатег description.
            $meta_description = $dom->find('meta[name="description"]', 0);
            if ($meta_description) {
                $this->description = $meta_description->content;
            }
        }

        $og_image = $dom->find('meta[property="og:image"]', 0);
        // Когда у russian.rt.com нет картинки, они пишут в тег просто http://russian.rt.com,
        // будем игнорировать это.
        if ($og_image && $og_image->content != 'http://russian.rt.com') {
            $imageUrl = $og_image->content;
            // Убедимся, что картинка по этому адресу существует.
            if (get_headers($imageUrl, 1)[0] == 'HTTP/1.1 200 OK')
                $this->imageUrl = $imageUrl;
        }

        // Переделаем заголовок и описание в utf-8, если они в другой кодировке.
        $charset = mb_detect_encoding($this->title, array('UTF-8', 'windows-1251'));
        if ($charset && $charset != 'UTF-8') {
            $this->title = iconv($charset, 'UTF-8', $this->title);
            $this->description = iconv($charset, 'UTF-8', $this->description);
        }
    }

    /*
     * Общий метод для загрузки упоминаний в соцсетях.
     */
    public function loadSocialMentions()
    {
        $this->loadTwitterMentions();
        $this->loadVkMentions();
        $this->loadFacebookMentions();
    }

    /*
     * Общий метод для загрузки информации о статье.
     */
    public function loadAllInformation()
    {
        $this->loadContent();
        $this->loadSocialMentions();
    }

    /*
     * Получение названия СМИ из адреса.
     */
    public function getPublisher()
    {
        preg_match('/\/\/(www\.)?([\w\.]+)\//', $this->url, $result);
        return $result[2];
    }

    /*
     * Проверка статьи на принадлежность к какому-нибудь сюжету.
     */
    public function belongsToStory()
    {
        return $this->storyId != null;
    }

    /*
     * Создание сюжета для этой статьи.
     */
    public function createStory($db)
    {
        $this->storyId = $db->createStory($this);
    }

    /*
     * Добавление статьи к существующему сюжету.
     */
    public function addToStory($storyId)
    {
        $this->storyId = $storyId;
    }
} 