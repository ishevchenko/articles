<html>
<head>
    <title>Статьи</title>
    <link rel="stylesheet" type="text/css" href="static/style.css">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>
<body>
    <div class="menu">
        <span><a href="/">Статьи</a></span>
        <span><a href="stories.php">Сюжеты</a></span>
    </div>
    {block name=body}{/block}
</body>
</html>