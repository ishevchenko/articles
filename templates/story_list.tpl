{extends file="../templates/base.tpl"}
{block name="body"}
    <h2>Последние сюжеты</h2>
    {foreach $articles as $article}
        <div class="story">
                <p>
                    <a href="stories.php?id={$article->storyId}">{$article->title}</a>
                </p>
        </div>
    {/foreach}
{/block}