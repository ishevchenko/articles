{extends file="../templates/base.tpl"}
{block name="body"}
    {foreach $articles as $article}
        <div class="article">
            {if $article->imageUrl}
                <img src="{$article->imageUrl}" class="article-image">
            {/if}
            <div class="article-content">
                <p>
                    <span>{$article->title}</span>
                    <span>(<a href="{$article->url}">{$article->getPublisher()}</a>)</span>
                </p>
                <p>
                    {$article->description}
                </p>
                <p>
                    <span class="social">{$article->facebookMentions} <img src="static/fb.png" class="social-icon"></span>
                    <span class="social">{$article->vkMentions} <img src="static/vk.png" class="social-icon"></span>
                    <span class="social">{$article->twitterMentions} <img src="static/tw.png" class="social-icon"></span>
                </p>
            </div>
        </div>
    {/foreach}
{/block}