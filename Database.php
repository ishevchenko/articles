<?php

require_once 'Article.php';

class Database
{

    private $connection;

    public function __construct($connectionString, $username, $password)
    {
        $this->connection = new PDO($connectionString, $username, $password);
    }

    /*
     * Преобразование полученной из БД строки в объект типа Article.
     */
    private function articlesFromRows($rows)
    {
        $articles = array();
        foreach ($rows as $row) {
            $articles[] = Article::fromDatabase($row['id'], $row['url'], $row['title'], $row['description'],
                $row['imageUrl'], $row['twitterMentions'], $row['facebookMentions'], $row['vkMentions'],
                $row['createdDate'], $row['updatedDate'], $row['storyId']);
        }
        return $articles;
    }

    /*
     * Создание новой статьи.
     */
    public function createArticle($article)
    {
        $query = $this->connection->prepare(
            'INSERT INTO article(url, title, description, imageUrl, twitterMentions, facebookMentions, vkMentions,
                                    createdDate, updatedDate, storyId)
             VALUES (:url, :title, :description, :imageUrl, :twitterMentions, :facebookMentions, :vkMentions,
                     :createdDate, :updatedDate, :storyId);');
        $params = (array)$article;
        unset($params['id']); // Удаление ненужного параметра id, равного null.
        $query->execute($params);
        return $this->connection->lastInsertId();
    }

    /*
     * Обновление статьи.
     */
    public function updateArticle($article)
    {
        $query = $this->connection->prepare(
            'UPDATE article
             SET url = :url, title = :title, description = :description, imageUrl = :imageUrl,
                 twitterMentions = :twitterMentions, facebookMentions = :facebookMentions, vkMentions = :vkMentions,
                 createdDate = :createdDate, updatedDate = :updatedDate, storyId = :storyId
             WHERE id = :id;');
        $query->execute((array)$article);
    }

    /*
     * Проверка url статьи на наличие в базе.
     */
    public function urlIsKnown($url)
    {
        $query = $this->connection->prepare('SELECT * FROM article WHERE url = :url');
        $query->execute(array(':url' => $url));
        $row_count = $query->rowCount();
        return $row_count > 0;
    }

    /*
     * Получение статей для обновления количества упоминаний в соцсетях.
     * Обновляются статьи не старше суток, со времени последнего обновления которых прошло больше часа.
     */
    public function getArticlesForUpdate()
    {
        $now = time();
        $createdAfter = date('Y-m-d H:i:s', strtotime('-1 day', $now));
        $updatedBefore = date('Y-m-d H:i:s', strtotime('-1 hour', $now));

        $query = $this->connection->prepare('SELECT * FROM article
                                             WHERE createdDate > :createdAfter AND updatedDate < :updatedBefore;');
        $query->execute(array(':createdAfter' => $createdAfter, ':updatedBefore' => $updatedBefore));

        return $this->articlesFromRows($query->fetchAll());
    }

    /*
     * Получение статей для вывода на сайте.
     * Отдаются 25 статей с наибольшим количеством упоминаний.
     */
    public function getArticlesForDisplay()
    {
        $query = $this->connection->prepare('SELECT * FROM article
                                             ORDER BY twitterMentions+facebookMentions+vkMentions DESC
                                             LIMIT 25;');
        $query->execute();
        return $this->articlesFromRows($query->fetchAll());
    }

    /*
     * Получение последних ста статей для поиска сюжетов.
     */
    public function getArticlesForStoryDetection()
    {
        $query = $this->connection->prepare('SELECT * FROM article
                                             ORDER BY createdDate DESC
                                             LIMIT 100;');
        $query->execute();
        return $this->articlesFromRows($query->fetchAll());
    }

    /*
     * Создание сюжета из статьи.
     */
    public function createStory($article)
    {
        $query = $this->connection->prepare(
            'INSERT INTO story(firstArticleId)
             VALUES (:articleId);');
        $query->execute(array('articleId' => $article->id));
        return $this->connection->lastInsertId();
    }

    /*
     * Получение последних 25 сюжетов для отображения на сайте.
     * Сюжеты отдаются в виде первой статьи, которая относится к ним.
     */
    public function getStoriesForDisplay()
    {
        $query = $this->connection->query(
            'SELECT * FROM story INNER JOIN article ON article.id = story.firstArticleId
            ORDER BY createdDate DESC LIMIT 25;');

        return $this->articlesFromRows($query->fetchAll());
    }

    /*
     * Получение статей, относящихся к сюжету.
     */
    public function getArticlesForStory($storyId)
    {
        $query = $this->connection->prepare('SELECT * FROM article WHERE storyId = :storyId
                                           ORDER BY createdDate;');
        $query->execute(array('storyId' => $storyId));

        return $this->articlesFromRows($query->fetchAll());
    }
}