CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(512) NOT NULL,
  `title` varchar(1024) NOT NULL,
  `description` text,
  `imageUrl` varchar(512),
  `twitterMentions` int(11),
  `facebookMentions` int(11),
  `vkMentions` int(11),
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL,
  `storyId` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `story`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstArticleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`firstArticleId`) REFERENCES `article`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `article`
  ADD CONSTRAINT `storyId` FOREIGN KEY (`storyId`) REFERENCES `story`(`id`) ON DELETE SET NULL